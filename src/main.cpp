#include <iostream>
#include <future>
#include <random>
#include <regex>
#include <string>

#include <cxxopts.hpp>
#include <INIReader.h>

#include "ldnn/data.hpp"

using namespace std::literals;

struct config_t {
    // The name of the csv that contains the input data
    std::string filename;

    // The dimension of the input vectors that contains the classification for
    // that vector.
    size_t classification_dimension;

    // The dimensions of the input vector the network should learn on (ignoring
    // the classification dimension)
    std::vector<size_t> dimensions;

    // Number of cross validation iterations.
    size_t iterations;

    // Number of gradient descent iterations.
    size_t gradient_iterations;
};

template<class T, class URBG>
auto random_partition(std::vector<T> vec, double p, URBG&& gen)
    -> std::pair<std::vector<T>, std::vector<T>>
{
    util::shuffle(vec, gen);
    auto split_at = std::next(begin(vec),
        static_cast<size_t>(0.5 * vec.size()));
    return std::make_pair(
        std::vector<T>(begin(vec), split_at),
        std::vector<T>(split_at, end(vec)));
}

auto read_config(const std::string& filename)
    -> config_t
{
    auto ini_config = INIReader{filename};
    if (ini_config.ParseError() < 0) {
        throw std::invalid_argument{filename + " couldn't be opened or parsed!"};
    }

    auto get_size_t = [&](const std::string& section, const std::string& name) {
        auto tmp = ini_config.GetInteger(section, name, -1);
        if (tmp < 0) {
            throw std::invalid_argument{"In config \'" + filename
                + "\' the name \'" + name + "\' in section \'" + section
                + "\' has to be set to a non-negative integer value!"};
        }
        return static_cast<size_t>(tmp);
    };
    auto get_string = [&](const std::string& section, const std::string& name) {
        auto tmp = ini_config.Get(section, name, "");
        if (tmp == "") {
            throw std::invalid_argument{"In config \'" + filename
                + "\' the name \'" + name + "\' in section \'" + section
                + "\' has to be set!"};
        }
        return tmp;
    };
    auto config = config_t{};

    config.filename = get_string("data", "filename");
    config.classification_dimension = get_size_t("data", "classification_dimension");

    auto dim_str = get_string("data", "dimensions");
    if (!std::regex_match(dim_str, std::regex{"\\[([0-9]+,)+[0-9]+\\]"}))
        throw std::invalid_argument{"The config value " + dim_str
            + " is not valid for parameter data.dimensions!"};
    auto r = std::regex{"[0-9]+"};
    std::for_each(
        std::sregex_iterator{dim_str.begin(), dim_str.end(), r},
        std::sregex_iterator{},
        [&](auto& match) {
            config.dimensions.push_back(std::stoul(match.str()));
        });

    config.iterations = get_size_t("training", "iterations");
    config.gradient_iterations = get_size_t("training", "gradient_iterations");

    return config;
}

int ldnn_main(int argc, char *argv[]) {
    auto cmdopt = cxxopts::Options{
        "ldnn", "C++ implementation of a Logistic Disjunctive Normal Network"};
    cmdopt.add_options()
        ("c,config", "ini config filename", cxxopts::value<std::string>());
    auto options = cmdopt.parse(argc, argv);
    auto config_filename = "ldnn.ini"s;
    if (options.count("config") > 0) {
        config_filename = options["config"].as<std::string>();
    }

    auto config = read_config(config_filename);

    std::random_device rd;
    std::mt19937 gen(rd());

    std::cout << "initializing...\r" << std::flush;

    // Load and parse the input data.
    auto data = ldnn::read_csv_file<double>(config.filename, '\t');
    auto examples = ldnn::dimension_to_classification(
        data, config.classification_dimension);
    for (auto& cl : examples) {
        cl.vec = ldnn::select_dimensions(cl.vec, config.dimensions);
    }

    // Normalize the input data.
    for (auto dim : indices<size_t>(examples[0].vec.rank().value)) {
        auto minmax = util::minmax(examples,
            [&](auto& c) { return c.vec[dim]; });
        for (auto& c : examples) {
            c.vec[dim] -= minmax.first;
            c.vec[dim] /= minmax.second - minmax.first;
        }
    }

    auto network_config = ldnn::network<double>::read_config(config_filename);
    auto perform_iteration = [&]() {
        auto partitioning = random_partition(examples, 0.5, gen);
        auto network = ldnn::network<double>(
            network_config, partitioning.first, gen);
        for (auto step : indices(config.gradient_iterations)) {
            (void)step;
            util::shuffle(partitioning.first, gen);
            network.gradient_descent(partitioning.first);
        }

        auto correct = size_t{0};
        for (auto& c : partitioning.second) {
            if ((network.classify(c.vec) > 0.5) == c.positive) {
                correct++;
            }
        }

        return static_cast<double>(correct) / partitioning.second.size();
    };

    std::cout << "calculating...\r" << std::flush;
    auto percentage_sum = 0.0;
    auto iterations = std::vector<std::future<double>>{};
    for (auto i : indices(config.iterations)) {
        (void) i;
        iterations.push_back(std::async(std::launch::async, perform_iteration));
    }
    for (auto& future : iterations) {
        future.wait();
        auto percentage = future.get();
        percentage_sum += percentage;
        std::cout << 100.0 * percentage
                  << "% correctly classified!\ncalculating...\r"
                  << std::flush;
    }
    std::cout << "On average "
              << 100.0 * percentage_sum / config.iterations
              << "% correctly classified!\n";

    return 0;
}

int main(int argc, char *argv[]) {
    try {
        return ldnn_main(argc, argv);
    }
    catch (const std::exception& e) {
        std::cout << "ERROR: " << e.what() << "\n";
    }
    catch(...) {
        std::cout << "an unexpected error occurred" << "\n";
    }
}
