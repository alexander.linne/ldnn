#pragma once

#include <climits>
#include <type_traits>

#include "ldnn/vector.hpp"

namespace ldnn {

template<class T = double>
class network {
    static_assert(std::is_floating_point<T>::value,
        "T has to be a floating-point type");

public:
    struct config_t {
        // Number of polytopes.
        size_t polytope_count;

        // Maximum number of halfspaces per polytope.
        size_t max_halfspaces;

        // Alpha parameter of the network.
        T alpha;

        // Number of iterations for the kmeans algorithm.
        size_t kmeans_iterations;
    };

    struct classification {
        vector<T> vec;
        bool positive;
    };

public:
    static config_t read_config(const std::string& filename) {
        auto ini_config = INIReader{filename};
        auto config = config_t{};

        auto get_long = [&](const std::string& section, const std::string& name) {
            auto tmp = ini_config.GetInteger(section, name, LONG_MIN);
            if (tmp == LONG_MIN) {
                throw std::invalid_argument{"In config \'" + filename
                    + "\' the name \'" + name + "\' in section \'" + section
                    + "\' has to be set to an integer value!"};
            }
            return tmp;
        };
        auto get_double = [&](const std::string& section, const std::string& name) {
            auto tmp = ini_config.GetReal(section, name, NAN);
            if (std::isnan(tmp)) {
                throw std::invalid_argument{"In config \'" + filename
                    + "\' the name \'" + name + "\' in section \'" + section
                    + "\' has to be set to a floating-point value!"};
            }
            return tmp;
        };

        config.polytope_count = get_long("network", "polytope_count");
        config.max_halfspaces = get_long("network", "max_halfspaces");
        config.alpha = get_double("network", "alpha");
        config.kmeans_iterations = get_long("network", "kmeans_iterations");

        return config;
    }

public:
    template<class URBG>
    network(config_t config, const std::vector<classification>& examples, URBG&& gen)
        : config(config)
    {
        if (examples.size() == 0)
            throw std::invalid_argument("examples.size() == 0");

        // Check that all input data has the same rank.
        auto rank = examples[0].vec.rank();
        for (auto& c : examples) {
            if (c.vec.rank() != rank) {
                throw std::invalid_argument(
                    "all examples must have the same rank");
            }
        }

        // Allocate memory.
        weight.resize(config.polytope_count);
        bias.resize(config.polytope_count);
        for (auto i : indices(config.polytope_count)) {
            weight[i].resize(config.max_halfspaces, vector<T>(rank));
            bias[i].resize(config.max_halfspaces);
        }

        // Initialize the network.
        auto pos_examples = std::vector<vector<T>>{};
        auto neg_examples = std::vector<vector<T>>{};
        util::for_each(examples, [&](auto& c) {
            if (c.positive) {
                pos_examples.push_back(c.vec);
            } else {
                neg_examples.push_back(c.vec);
            }
        });
        auto pos_ctrds = kmeans(pos_examples,
            config.polytope_count, gen, config.kmeans_iterations);
        auto neg_ctrds = kmeans(neg_examples,
            config.max_halfspaces, gen, config.kmeans_iterations);
        for (auto i : indices(pos_ctrds.size())) {
            for (auto j : indices(neg_ctrds.size())) {
                weight[i][j] = normalize(pos_ctrds[i] - neg_ctrds[j]);
                bias[i][j] = weight[i][j] * (0.5 * (pos_ctrds[i] + neg_ctrds[j]));
            }
        }
    }

    auto classify(const vector<T>& v)
        -> T
    {
        auto result = T{1};
        for (auto i : indices(config.polytope_count)) {
            result *= T{1} - polytope(i, v);
        }

        return T{1} - result;
    }

    void gradient_descent(const classification& c) {
        for (auto i : indices(config.polytope_count)) {
            for (auto j : indices(config.max_halfspaces)) {
                auto diff = T{2} * error(c);

                for (auto r : indices(config.polytope_count)) {
                    if (i != r) {
                        diff *= (T{1} - polytope(r, c.vec));
                    }
                }

                diff *= polytope(i, c.vec) * (T{1} - halfspace(i, j, c.vec));
                diff *= config.alpha;

                weight[i][j] = weight[i][j] - (diff * c.vec);
                bias[i][j] -= diff;
            };
        }
    }

    template<class Range,
        class = typename std::enable_if<
            std::is_convertible<
                typename std::decay<Range>::type::value_type,
                classification
            >::value
        >::type
    >
    void gradient_descent(Range&& rng) {
        util::for_each(rng, [&](auto& c) { gradient_descent(c); });
    }

    T quadratic_error(const classification& c) {
        return util::square(error(c));
    }

    template<class Range,
        class = typename std::enable_if<
            std::is_convertible<
                typename std::decay<Range>::type::value_type,
                classification
            >::value
        >::type
    >
    T quadratic_error(Range&& data) {
        auto error = std::vector<T>{};
        util::transform(data, std::back_inserter(error),
            [&](auto& c) { return quadratic_error(c); });
        return util::accumulate(error, T{0});
    }

private:
    T error(const classification& c) {
        return classify(c.vec) - (c.positive ? T{1} : T{0});
    }

    template<class URBG>
    static auto kmeans(std::vector<vector<T>> data,
        size_t k, URBG&& gen, size_t iterations = 10)
        -> std::vector<vector<T>>
    {

        // There have to be at least as many data elements as the number
        // of clusters to be calculated.
        if (k > data.size()) {
            throw std::invalid_argument("too many clusters for given data");
        }

        auto centroids = std::vector<vector<T>>(k);


        auto initialize_centroids = [&]() {
            // Shuffle the input vector and initialize centroids.
            util::shuffle(data, std::forward<URBG>(gen));
            util::copy_n(data, k, begin(centroids));
        };
        initialize_centroids();

        // Iterate
        auto clusters = std::vector<std::vector<vector<T>>>(k);
        auto add_to_nearest_cluster = [&](const auto& vec) {
            auto dist = std::vector<T>(centroids.size());
            util::transform(centroids, begin(dist),
                [&](auto& c) { return distance(vec, c); });
            clusters[util::index(dist, util::min_element(dist))].push_back(vec);
        };
        for (auto it = size_t{1}; it <= iterations; ++it) {
            util::for_each(data, add_to_nearest_cluster);
            bool has_empty = false;
            for (auto& cluster : clusters)
                has_empty |= cluster.empty();
            if (has_empty) {
                initialize_centroids();
                it = 0;
            } else {
                util::transform(clusters, centroids.begin(),
                    [](auto& cluster) { return centroid(cluster); });
            }
            util::for_each(clusters, [](auto& v) { v.clear(); });
        }

        return centroids;
    }

    auto halfspace(size_t i, size_t j, const vector<T>& v)
        -> T
    {
        auto denom = T{1} + std::exp(-(weight[i][j] * v) - bias[i][j]);

        // std::exp may return inf: print an error and return 1 / inf ~ 0
        if (std::isinf(denom)) {
            std::cerr << "invalid denom: " << denom
                << ", i: " << i << ", j: " << j
                << ", weight * v: " << (weight[i][j] * v)
                << ", bias: " << bias[i][j]
                << "\n";
            return T{0};
        }

        return T{1} / denom;
    }

    auto polytope(size_t i, const vector<T>& v)
        -> T
    {
        auto result = T{1};
        for (auto j : indices(config.max_halfspaces)) {
            result *= halfspace(i, j, v);
        }
        return result;
    }

private:

    config_t config;
    std::vector<std::vector<vector<T>>> weight;
    std::vector<std::vector<T>> bias;
};

} // namespace ldnn
